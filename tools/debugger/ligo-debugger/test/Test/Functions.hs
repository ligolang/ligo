-- | Unit tests on functionality related to LIGO lambdas.
module Test.Functions
  ( module Test.Functions
  ) where

import Test.Tasty (TestTree, testGroup)

test_LambdaMeta :: TestTree
test_LambdaMeta = testGroup "functionality around LambdaMeta"
  [ -- no tests ATM
  ]
