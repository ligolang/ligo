(*
  This is a comment.
*)
(* This is a multi-line comment.
  (* This is a "nested" comment. *)
*)
let x = 10 // This is a single line comment